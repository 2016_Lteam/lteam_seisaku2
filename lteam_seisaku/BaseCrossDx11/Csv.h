#pragma once
#include "stdafx.h"

namespace basecross {
	class Csv : public GameObject
	{
		UINT m_StageNum;	//ステージ番号

		int* m_stagearr;	//ステージ配列(オブジェクト)
		//int* m_stageEnemy;	//ステージ配列(エネミー用)

		Vector3 m_PlayerinitPos;				//プレイヤーの初期位置
		Vector2 m_StageSize;					//xが横、yが縦

		bool m_Pflg = false;					//プレイヤーが多重で設定されていないか確認用
		//bool m_Gflg = false;					//ゴール板
		const Vector2 m_PanelSize = Vector2(256, 256);		//パネルサイズ
	public:
		Csv(const shared_ptr<Stage>& StagePtr, UINT StageNum);
		virtual ~Csv();

		virtual void OnCreate()override;

		//ステージ配列(当たり判定)を渡す
		int* GetStageArr() { return m_stagearr; }
		//ステージの大きさを渡す
		Vector2 GetStageSize() { return m_StageSize; }

		//プレイヤーの初期位置を返す
		Vector3 GetPlayerInitPos() { return m_PlayerinitPos; }
		
	};
}
