/*!
@file Dog.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Dog : public GameObject;
	//	用途: 犬
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Dog::Dog(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),	//大きさ
		m_Rot(Rotation),//回転
		m_Pos(Position)	//位置
	{}

	//初期化
	void Dog::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rot);
		Ptr->SetPosition(m_Pos);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();

		//最下地点
		PtrGravity->SetBaseY(0.0f);
		//衝突判定をつける
		//auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		//PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DOG_MESH");
		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DOG_MESH");
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//透明処理
		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Dog> >(GetThis<Dog>());
		//最初のステートをD_DefaultStateに設定
		m_StateMachine->ChangeState(D_DefaultState::Instance());

		SetDrawLayer(1);

	}


	//更新
	void Dog::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	void Dog::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		//スパークの放出
		auto PtrSpark = GetStage()->GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
		if (PtrSpark) {
			PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition());
		}
		if (GetStateMachine()->GetCurrentState() == SearchState::Instance()) {
			GetStateMachine()->ChangeState(D_DefaultState::Instance());
		}
	}

	//ターンの最終更新時
	void Dog::OnLastUpdate() {
	}

	//playerの前座標に犬が移動する
	void Dog::SetPlayer_BeforePos(Vector3 Player_Pos) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto P_Move_Direction = PlayerPtr->m_Dog_MoveAngle;
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto P_Pos = PTrans->GetPosition();
		auto Move_Speed = PlayerPtr->Move_Speed;
		auto D_Trans = GetComponent<Transform>();
		auto D_Pos = D_Trans->GetPosition();

		//D_Trans->SetPosition(Player_Pos);

		switch (P_Move_Direction)
		{
		case 0:
			m_Move_Direction = 0;
			break;
		case 1:
			m_Move_Direction = 1;
			break;
		case 2:
			m_Move_Direction = 2;
			break;
		case 3:
			m_Move_Direction = 3;
			break;
		}
		m_Dog_Walk = true;
	}

	void Dog::Dog_Move_Now(int MD) {
		//プレイヤーの情報を呼び出す
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto p_Pos = PTrans->GetPosition();
		auto Move_Speed = PlayerPtr->Move_Speed;
		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//ドアグループを呼び出す
		auto dgPtr = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		//犬のTransformを取得する
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		//MDに応じて移動方向を変化させる
		switch (MD)
		{
		case 0:
			for (auto v : ogoPtr->GetGroupVector()) {
				auto goptr = v.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == d_Pos.x&&goPos.z == d_Pos.z + 1) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == d_Pos.x&&dPos.z == d_Pos.z + 1 && dptr->m_Open == false) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
			if (p_Pos.x == d_Pos.x&&p_Pos.z == d_Pos.z + 1) {
				m_Dog_Walk = false;
				m_Dog_SWalk = false;

			}
			if (m_Dog_Walk||m_Dog_SWalk) {
				d_Trans->SetRotation(0, 3.14f, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x, d_Pos.y, d_Pos.z + Move_Speed));
			}
			break;
		case 1:
			for (auto v : ogoPtr->GetGroupVector()) {
				auto goptr = v.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == d_Pos.x&&goPos.z == d_Pos.z - 1) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;

					break;
				}
			}
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == d_Pos.x&&dPos.z == d_Pos.z - 1 && dptr->m_Open == false) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
			if (p_Pos.x == d_Pos.x&&p_Pos.z == d_Pos.z - 1) {
				m_Dog_Walk = false;
				m_Dog_SWalk = false;

			}
			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, 0, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x, d_Pos.y, d_Pos.z - Move_Speed));
			}
			break;
		case 2:
			for (auto v : ogoPtr->GetGroupVector()) {
				auto goptr = v.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == d_Pos.x - 1 &&goPos.z == d_Pos.z) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;

					break;
				}
			}
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == d_Pos.x - 1 &&dPos.z == d_Pos.z  && dptr->m_Open == false) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
			if (p_Pos.x == d_Pos.x - 1 &&p_Pos.z == d_Pos.z) {
				m_Dog_Walk = false;
				m_Dog_SWalk = false;

			}
			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, 3.14f / 2, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x - Move_Speed, d_Pos.y, d_Pos.z));
			}
			break;
		case 3:
			for (auto v : ogoPtr->GetGroupVector()) {
				auto goptr = v.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == d_Pos.x + 1 && goPos.z == d_Pos.z) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;

					break;
				}
			}
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == d_Pos.x + 1 && dPos.z == d_Pos.z  && dptr->m_Open == false) {
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
			if (p_Pos.x == d_Pos.x + 1 && p_Pos.z == d_Pos.z) {
				m_Dog_Walk = false;
				m_Dog_SWalk = false;
			}
			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, -3.14f/2, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x + Move_Speed, d_Pos.y, d_Pos.z));
			}
			break;
		}
		//m_MoveDistanceにMove_Speedを毎ターン格納する
		m_Move_Distance += Move_Speed;

		//m_Move_Distanceが1.0f(1マスの移動を終えたら)
		if (m_Move_Distance >= 1.0f) {
			m_Move_Distance = 0;
			d_Trans->SetPosition(Vector3(roundf(d_Pos.x), d_Pos.y, roundf(d_Pos.z)));
			m_Dog_Walk = false;
			m_Dog_SWalk = false;
			//カメラの回転量を把握するための表示
		}
		auto RPGptr = GetStage()->GetSharedObjectGroup(L"RoadPathGroup");


		////wstring str = L"x : " + Util::FloatToWStr(m_Path_History.back()->GetComponent<Transform>()->GetPosition().x, 6, Util::FloatModify::Fixed)+ L"\ny : " + Util::FloatToWStr(m_Path_History.back()->GetComponent<Transform>()->GetPosition().z, 6, Util::FloatModify::Fixed);
		////wstring str = L"x : " + Util::FloatToWStr(RPGptr->size(), 6, Util::FloatModify::Fixed);
		//wstring str = L"x : " + Util::FloatToWStr(d_Rot.y, 6, Util::FloatModify::Fixed);
		////文字列をつける
		//	auto PtrString = GetComponent<StringSprite>();
		//	PtrString->SetText(str);

}

	//一番近いプレゼントボックスを探す関数
	void Dog::Search_Treasure() {
		//距離判定用に使う変数
		float storage1 = 0;
		float storage2 = 0;
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		//PresentBoxGroupの情報を取得する
		auto PBGptr = GetStage()->GetSharedObjectGroup(L"PresentBoxGroup");
		for (auto v : PBGptr->GetGroupVector()) {
			auto pb = dynamic_pointer_cast<PresentBoxModel>(v.lock());
			auto pbtrans = pb->GetComponent<Transform>();
			auto pb_Pos = pbtrans->GetPosition();
			if (storage1 == 0) {
				storage1 = sqrt((d_Pos.x - pb_Pos.x)*(d_Pos.x - pb_Pos.x) + (d_Pos.z - pb_Pos.z)*(d_Pos.z - pb_Pos.z));
				m_pb_storage = pb;
			}
			else {
				storage2 = sqrt((d_Pos.x - pb_Pos.x)*(d_Pos.x - pb_Pos.x) + (d_Pos.z - pb_Pos.z)*(d_Pos.z - pb_Pos.z));
				if (storage1 > storage2) {
					storage1 = storage2;
					m_pb_storage = pb;
				}
			}
		}
		m_TargetTreasure = true;
		m_Path_History.clear();
	}

	//犬がいずれかのパスの座標にいるかを確認して、いずれのパスの座標にもいなかった場合
	//もしくはパスの上にのっている時、近くのパスの座標に移動するためパスの情報を格納する関数
	void Dog::Dog_SearchNearpath() {
		//距離判定用に使う変数
		float storage1 = 0;
		float storage2 = 0;
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();

		//RoadPathGroupの情報を取得する
		auto RPGptr = GetStage()->GetSharedObjectGroup(L"RoadPathGroup");
		for (auto v : RPGptr->GetGroupVector()) {
			auto rp = dynamic_pointer_cast<RoadPath>(v.lock());
			auto rptrans = rp->GetComponent<Transform>();
			auto rp_Pos = rptrans->GetPosition();
			////for文に使用
			//int Replacement_X_High;
			//int Replacement_X_Low;
			//int Replacement_Z_High;
			//int Replacement_Z_Low;
			//auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
			////自分と検索したパス間にオブジェクトがないか判断
			if (m_phCounter > 0) {
				d_Pos = m_rp_storage->GetComponent<Transform>()->GetPosition();
			//	if (d_Pos.x < rp_Pos.x) {
			//		Replacement_X_High = (int)rp_Pos.x;
			//		Replacement_X_Low = (int)d_Pos.x;
			//	}
			//	else {
			//		Replacement_X_High = (int)d_Pos.z;
			//		Replacement_X_Low = (int)rp_Pos.z;
			//	}
			//	if (d_Pos.z < rp_Pos.z) {
			//		Replacement_Z_High = (int)rp_Pos.z;
			//		Replacement_Z_Low = (int)d_Pos.z;
			//	}
			//	else {
			//		Replacement_Z_High = (int)d_Pos.z;
			//		Replacement_Z_Low = (int)rp_Pos.z;
			//	}
			//	for (int i = Replacement_X_Low; i <= Replacement_X_High; i++) {
			//		for (int j = Replacement_Z_High; j <= Replacement_Z_Low; j--) {
			//			for (auto ogo : ogoPtr->GetGroupVector()) {
			//				auto go = ogo.lock();
			//				auto goTrans = go->GetComponent<Transform>();
			//				auto go_Pos = goTrans->GetPosition();
			//				if (go_Pos.x == i && go_Pos.z == j) {
			//					m_Hindrance = true;
			//					break;
			//				}
			//			}
			//			if (m_Hindrance == true) {
			//				break;
			//			}
			//		}
			//		if (m_Hindrance == true) {
			//			break;
			//		}
			//	}
			}
			if (m_phCounter > 0) {
				for (int i = 0; i < m_phCounter; i++) {
					if (m_Path_History[i] == rp) {
						m_Hindrance = true;
					}
				}
			}
			if (!m_Hindrance) {
					if (m_phCounter > 0) {
						if (d_Pos.x == rp_Pos.x || d_Pos.z == rp_Pos.z) {
							storage1 = sqrt((d_Pos.x - rp_Pos.x)*(d_Pos.x - rp_Pos.x) + (d_Pos.z - rp_Pos.z)*(d_Pos.z - rp_Pos.z));
							m_rp_storage = rp;
							break;
						}

					}
					else{
						storage1 = sqrt((d_Pos.x - rp_Pos.x)*(d_Pos.x - rp_Pos.x) + (d_Pos.z - rp_Pos.z)*(d_Pos.z - rp_Pos.z));
						m_rp_storage = rp;
					}
			}
			m_Hindrance = false;
		}
		m_Path_History.push_back(m_rp_storage);
		m_DogOnthePath = true;
		m_phCounter++;
	}
	//自分とパスの座標情報を取得し移動方向を決める(X軸とY軸を合わせる感じー)
	void Dog::DogToPath_MoveAngle() {
		//事前に取得しているパスの情報
			auto rp = m_rp_storage;
			auto rptrans = rp->GetComponent<Transform>();
			auto rp_Pos = rptrans->GetPosition();
			//事前に取得しているプレゼントボックスの情報
			auto pb = m_pb_storage;
			auto pbtrans = pb->GetComponent<Transform>();
			auto pb_Pos = pbtrans->GetPosition();
				//犬の位置情報を格納
				auto d_Trans = GetComponent<Transform>();
				auto d_Pos = d_Trans->GetPosition();
				auto d_Rot = d_Trans->GetRotation();

				if (!m_PathToPresentBox) {
					if (rp_Pos.x == d_Pos.x && rp_Pos.z == d_Pos.z) {
						auto pb_Pos = m_pb_storage->GetComponent<Transform>()->GetPosition();
						if (pb_Pos.x == rp_Pos.x || pb_Pos.z == rp_Pos.z) {
							m_PathToPresentBox = true;
						}
						else {
							m_DogOnthePath = false;
						}
					}
					else {
						if (rp_Pos.z > d_Pos.z) {
							m_Move_Direction = 0;
						}
						else if (rp_Pos.z < d_Pos.z) {
							m_Move_Direction = 1;
						}
						else if (rp_Pos.x < d_Pos.x) {
							m_Move_Direction = 2;
						}
						else if (rp_Pos.x > d_Pos.x) {
							m_Move_Direction = 3;
						}

						m_Dog_SWalk = true;
					}
				}
				else{
					if (pb_Pos.z > d_Pos.z) {
						m_Move_Direction = 0;
					}
					else if (pb_Pos.z < d_Pos.z) {
						m_Move_Direction = 1;
					}
					else if (pb_Pos.x < d_Pos.x) {
						m_Move_Direction = 2;
					}
					else if (pb_Pos.x > d_Pos.x) {
						m_Move_Direction = 3;
					}

					m_Dog_SWalk = true;
				}
}


	//--------------------------------------------------------------------------------------
	//	class D_DefaultState : public ObjState<Dog>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<D_DefaultState> D_DefaultState::Instance() {
		static shared_ptr<D_DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<D_DefaultState>(new D_DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void D_DefaultState::Enter(const shared_ptr<Dog>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void D_DefaultState::Execute(const shared_ptr<Dog>& Obj) {
		if (Obj->m_Dog_Walk) {
			Obj->Dog_Move_Now(Obj->m_Move_Direction);
		}
		if (Obj->m_Dog_OrderNow) {
			Obj->GetStateMachine()->ChangeState(SearchState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void D_DefaultState::Exit(const shared_ptr<Dog>& Obj) {
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class SearchState : public ObjState<Dog>;
	//	用途: 探索状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<SearchState> SearchState::Instance() {
		static shared_ptr<SearchState> instance;
		if (!instance) {
			instance = shared_ptr<SearchState>(new SearchState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void SearchState::Enter(const shared_ptr<Dog>& Obj) {
	}
	//ステート実行中に毎ターン呼ばれる関数
	void SearchState::Execute(const shared_ptr<Dog>& Obj) {
		if (!Obj->m_Dog_OrderNow) {
			Obj->m_TargetTreasure = false;
			Obj->m_Dog_SWalk = false;
			Obj->m_DogOnthePath = false;
			Obj->GetStateMachine()->ChangeState(D_DefaultState::Instance());
		}
		if (Obj->m_Dog_OrderNow) {
			if (!Obj->m_TargetTreasure) {
				Obj->Search_Treasure();
				Obj->m_phCounter = 0;
			}
			if (!Obj->m_DogOnthePath) {
				Obj->Dog_SearchNearpath();
				Obj->m_phCounter++;
			}
			else if (Obj->m_DogOnthePath) {
				if (!Obj->m_Dog_SWalk) {
					Obj->DogToPath_MoveAngle();
				}
				if (Obj->m_Dog_SWalk) {
					Obj->Dog_Move_Now(Obj->m_Move_Direction);
				}
			}
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void SearchState::Exit(const shared_ptr<Dog>& Obj) {
	}



}
//end basecross

