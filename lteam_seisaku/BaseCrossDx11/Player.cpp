/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),	//大きさ
		m_Rot(Rotation),//回転
		m_Pos(Position)	//位置
	{}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rot);
		Ptr->SetPosition(m_Pos);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();

		//最下地点
		PtrGravity->SetBaseY(0.0f);
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DOG_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DOG_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//透明処理
		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState::Instance());
		m_BeforePos = GetComponent<Transform>()->GetPosition();

		SetDrawLayer(1);

	}


	void Player::Confirmation_Pos() {
		//プレイヤーの最初の一歩を取得する
		//if (m_first_Walk) {
			auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			auto dog_pos = dogptr->GetComponent<Transform>()->GetPosition();
			auto P_Pos = GetComponent<Transform>()->GetPosition();
			if (m_BeforePos.x == P_Pos.x && m_BeforePos.z == P_Pos.z) {
			}
			else {
				dogptr->SetPlayer_BeforePos(m_BeforePos);
			}
			m_BeforePos = P_Pos;
		//}
	}


	//更新
	void Player::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	void Player::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		//スパークの放出
		auto PtrSpark = GetStage()->GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
		if (PtrSpark) {
			PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition());
		}
	}
	//ターンの最終更新時
	void Player::OnLastUpdate() {
	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player::MoveMotion(){
		//カメラが一回転するのに必要な回転量
		auto One_revolution_Camera = 6.2f;

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].fThumbRX != 0) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			m_C_Eye += CntlVec[0].fThumbRX * ElapsedTime;
			 if (m_C_Eye > One_revolution_Camera || m_C_Eye < -One_revolution_Camera) {
				 m_C_Eye = 0;
			 }
			 //カメラの回転量を把握するための表示
			 wstring str = L"CAMERAEYE\n :" + Util::FloatToWStr(m_C_Eye, 6, Util::FloatModify::Fixed);
			 //文字列をつける
			 auto PtrString = GetComponent<StringSprite>();
			 PtrString->SetText(str);
		}
		//コントローラーの十字ボタンを変数に代入しておく

		auto Dpad_Key1 = XINPUT_GAMEPAD_DPAD_UP;
		auto Dpad_Key2 = XINPUT_GAMEPAD_DPAD_DOWN;
		auto Dpad_Key3 = XINPUT_GAMEPAD_DPAD_LEFT;
		auto Dpad_Key4 = XINPUT_GAMEPAD_DPAD_RIGHT;

		//キーボードとマウスの取得
		auto Key = App::GetApp()->GetInputDevice().GetKeyState();

		auto key1 = VK_UP;
		auto key2 = VK_DOWN;
		auto key3 = VK_LEFT;
		auto key4 = VK_RIGHT;


		//プレイヤーが移動していなければ
		if (!m_Player_Walk) {
			if (m_C_Eye > 0) {
				Dpad_Key1 = XINPUT_GAMEPAD_DPAD_UP;
				Dpad_Key2 = XINPUT_GAMEPAD_DPAD_DOWN;
				Dpad_Key3 = XINPUT_GAMEPAD_DPAD_LEFT;
				Dpad_Key4 = XINPUT_GAMEPAD_DPAD_RIGHT;
				if (m_C_Eye > One_revolution_Camera / 4) {
					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_RIGHT;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_LEFT;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_UP;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_DOWN;
					if (m_C_Eye > One_revolution_Camera / 2) {
						Dpad_Key1 = XINPUT_GAMEPAD_DPAD_DOWN;
						Dpad_Key2 = XINPUT_GAMEPAD_DPAD_UP;
						Dpad_Key3 = XINPUT_GAMEPAD_DPAD_RIGHT;
						Dpad_Key4 = XINPUT_GAMEPAD_DPAD_LEFT;
						if (m_C_Eye > One_revolution_Camera / 4 * 3) {
							Dpad_Key1 = XINPUT_GAMEPAD_DPAD_LEFT;
							Dpad_Key2 = XINPUT_GAMEPAD_DPAD_RIGHT;
							Dpad_Key3 = XINPUT_GAMEPAD_DPAD_DOWN;
							Dpad_Key4 = XINPUT_GAMEPAD_DPAD_UP;
						}
					}
				}
			}
			else if (m_C_Eye < 0) {
				//コントローラーの十字ボタンを変数に代入しておく
				Dpad_Key1 = XINPUT_GAMEPAD_DPAD_UP;
				Dpad_Key2 = XINPUT_GAMEPAD_DPAD_DOWN;
				Dpad_Key3 = XINPUT_GAMEPAD_DPAD_LEFT;
				Dpad_Key4 = XINPUT_GAMEPAD_DPAD_RIGHT;
				if (m_C_Eye < -One_revolution_Camera / 4 + 0.01f) {

					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_LEFT;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_RIGHT;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_DOWN;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_UP;
					if (m_C_Eye < -One_revolution_Camera / 2 + 0.01f) {
						Dpad_Key1 = XINPUT_GAMEPAD_DPAD_DOWN;
						Dpad_Key2 = XINPUT_GAMEPAD_DPAD_UP;
						Dpad_Key3 = XINPUT_GAMEPAD_DPAD_RIGHT;
						Dpad_Key4 = XINPUT_GAMEPAD_DPAD_LEFT;
						if (m_C_Eye < -One_revolution_Camera / 4 * 3 + 0.01f) {
							Dpad_Key1 = XINPUT_GAMEPAD_DPAD_RIGHT;
							Dpad_Key2 = XINPUT_GAMEPAD_DPAD_LEFT;
							Dpad_Key3 = XINPUT_GAMEPAD_DPAD_UP;
							Dpad_Key4 = XINPUT_GAMEPAD_DPAD_DOWN;
						}
					}
				}
			}

			auto Dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			auto d_trans = Dogptr->GetComponent<Transform>();
			auto d_Pos = d_trans->GetPosition();
			auto p_Trans = GetComponent<Transform>();
			auto p_Pos = p_Trans->GetPosition();

			//コントローラの取得
			if (!(Dogptr->m_Dog_Walk)) {
				if (Key.m_bPushKeyTbl[key1]) {
					//p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + 1.0f));
					m_Move_Direction = 0;
					if (d_Pos.x < p_Pos.x) {
						m_Dog_MoveAngle = 3;
					}
					if (d_Pos.x > p_Pos.x) {
						m_Dog_MoveAngle = 2;
					}
					if (d_Pos.z > p_Pos.z) {
						m_Dog_MoveAngle = 1;
					}
					if (d_Pos.z < p_Pos.z) {
						m_Dog_MoveAngle = 0;
					}
					m_first_Walk = true;
					m_Player_Walk = true;
				}
				if (Key.m_bPushKeyTbl[key2]) {
					//p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - 1.0f));
					m_Move_Direction = 1;
					if (d_Pos.x < p_Pos.x) {
						m_Dog_MoveAngle = 3;
					}
					if (d_Pos.x > p_Pos.x) {
						m_Dog_MoveAngle = 2;
					}
					if (d_Pos.z > p_Pos.z) {
						m_Dog_MoveAngle = 1;
					}
					if (d_Pos.z < p_Pos.z) {
						m_Dog_MoveAngle = 0;
					}
					m_first_Walk = true;
					m_Player_Walk = true;
				}
				if (Key.m_bPushKeyTbl[key3]) {
					//p_Trans->SetPosition(Vector3(p_Pos.x - 1.0f, p_Pos.y, p_Pos.z));
					m_Move_Direction = 2;
					if (d_Pos.x < p_Pos.x) {
						m_Dog_MoveAngle = 3;
					}
					if (d_Pos.x > p_Pos.x) {
						m_Dog_MoveAngle = 2;
					}
					if (d_Pos.z > p_Pos.z) {
						m_Dog_MoveAngle = 1;
					}
					if (d_Pos.z < p_Pos.z) {
						m_Dog_MoveAngle = 0;
					}
					m_first_Walk = true;
					m_Player_Walk = true;
				}
				if (Key.m_bPushKeyTbl[key4]) {
					//p_Trans->SetPosition(Vector3(p_Pos.x + 1.0f, p_Pos.y, p_Pos.z));
					m_Move_Direction = 3;
					if (d_Pos.x < p_Pos.x) {
						m_Dog_MoveAngle = 3;
					}
					if (d_Pos.x > p_Pos.x) {
						m_Dog_MoveAngle = 2;
					}
					if (d_Pos.z > p_Pos.z) {
						m_Dog_MoveAngle = 1;
					}
					if (d_Pos.z < p_Pos.z) {
						m_Dog_MoveAngle = 0;
					}
					m_first_Walk = true;
					m_Player_Walk = true;
				}



				if (CntlVec[0].bConnected) {
						if (CntlVec[0].wButtons & Dpad_Key1) {
							//p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + 1.0f));
							m_Move_Direction = 0;
							if (d_Pos.x < p_Pos.x) {
								m_Dog_MoveAngle = 3;
							}
							if (d_Pos.x > p_Pos.x) {
								m_Dog_MoveAngle = 2;
							}
							if (d_Pos.z > p_Pos.z) {
								m_Dog_MoveAngle = 1;
							}
							if (d_Pos.z < p_Pos.z) {
								m_Dog_MoveAngle = 0;
							}
							m_first_Walk = true;
							m_Player_Walk = true;
						}
						if (CntlVec[0].wButtons & Dpad_Key2) {
							//p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - 1.0f));
							m_Move_Direction = 1;
							if (d_Pos.x < p_Pos.x) {
								m_Dog_MoveAngle = 3;
							}
							if (d_Pos.x > p_Pos.x) {
								m_Dog_MoveAngle = 2;
							}
							if (d_Pos.z > p_Pos.z) {
								m_Dog_MoveAngle = 1;
							}
							if (d_Pos.z < p_Pos.z) {
								m_Dog_MoveAngle = 0;
							}
							m_first_Walk = true;
							m_Player_Walk = true;
						}
						if (CntlVec[0].wButtons & Dpad_Key3) {
							//p_Trans->SetPosition(Vector3(p_Pos.x - 1.0f, p_Pos.y, p_Pos.z));
							m_Move_Direction = 2;
							if (d_Pos.x < p_Pos.x) {
								m_Dog_MoveAngle = 3;
							}
							if (d_Pos.x > p_Pos.x) {
								m_Dog_MoveAngle = 2;
							}
							if (d_Pos.z > p_Pos.z) {
								m_Dog_MoveAngle = 1;
							}
							if (d_Pos.z < p_Pos.z) {
								m_Dog_MoveAngle = 0;
							}
							m_first_Walk = true;
							m_Player_Walk = true;
						}
						if (CntlVec[0].wButtons & Dpad_Key4) {
							//p_Trans->SetPosition(Vector3(p_Pos.x + 1.0f, p_Pos.y, p_Pos.z));
							m_Move_Direction = 3;
							if (d_Pos.x < p_Pos.x) {
								m_Dog_MoveAngle = 3;
							}
							if (d_Pos.x > p_Pos.x) {
								m_Dog_MoveAngle = 2;
							}
							if (d_Pos.z > p_Pos.z) {
								m_Dog_MoveAngle = 1;
							}
							if (d_Pos.z < p_Pos.z) {
								m_Dog_MoveAngle = 0;
							}
							m_first_Walk = true;
							m_Player_Walk = true;
						}

					}	
				}
			}
		//プレイヤーが移動していたら
		else if(m_Player_Walk){
			Player_Move_Now(m_Move_Direction);
		}

	}

	void Player::Player_Move_Now(int MD) {
		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//PlayerのTransformを取得する
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto p_Trans = GetComponent<Transform>();
		auto p_Pos = p_Trans->GetPosition();
		//MDに応じて移動方向を変化させる
		switch (MD)
		{
		case 0:
			for (auto v : ogoPtr->GetGroupVector()) {
				auto goptr = v.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == p_Pos.x&&goPos.z ==  p_Pos.z + 1) {
					m_Player_Walk = false;
					break;
				}
			}
			if (m_Player_Walk) {
				p_Trans->SetRotation(0, 3.14f, 0);
				p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + Move_Speed));
			}
			break;
		case 1:
			for (auto go : ogoPtr->GetGroupVector()) {
				auto goptr = go.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == p_Pos.x&&goPos.z == p_Pos.z - 1) {
					m_Player_Walk = false;
					break;
				}
			}
			if (m_Player_Walk) {
				p_Trans->SetRotation(0, 0, 0);
				p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - Move_Speed));
			}
			break;
		case 2:
			for (auto go : ogoPtr->GetGroupVector()) {
				auto goptr = go.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == p_Pos.x-1&&goPos.z == p_Pos.z ) {
					m_Player_Walk = false;
					break;
				}
			}
			if (m_Player_Walk) {
				p_Trans->SetRotation(0, 3.14f/2, 0);
				p_Trans->SetPosition(Vector3(p_Pos.x - Move_Speed, p_Pos.y, p_Pos.z));
			}
			break;
		case 3:
			for (auto go : ogoPtr->GetGroupVector()) {
				auto goptr = go.lock();
				auto goTrans = goptr->GetComponent<Transform>();
				auto goPos = goTrans->GetPosition();
				if (goPos.x == p_Pos.x+1&&goPos.z == p_Pos.z) {
					m_Player_Walk = false;
					break;
				}
			}
			if (m_Player_Walk) {
				p_Trans->SetRotation(0, -3.14f/2, 0);
				p_Trans->SetPosition(Vector3(p_Pos.x + Move_Speed, p_Pos.y, p_Pos.z));
			}
			break;
		}
		//m_MoveDistanceにMove_Speedを毎ターン格納する
		m_Move_Distance += Move_Speed;

		//m_Move_Distanceが1.0f(1マスの移動を終えたら)
		if (m_Move_Distance >= 1.0f) {
			m_Move_Distance = 0;
			p_Trans->SetPosition(Vector3(roundf(p_Pos.x), p_Pos.y, roundf(p_Pos.z)));
			m_Player_Walk = false;
			auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			if (dogptr->m_Dog_OrderNow == false) {
				Confirmation_Pos();
			}
		}
	}

	void Player::PlayerToDog_Order() {
		auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (!dogptr->m_Dog_Walk) {
			if (CntlVec[0].bConnected) {
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
				{
					if (!dogptr->m_Dog_OrderNow) {
						dogptr->m_Dog_OrderNow = true;
						m_Dog_OrderNow = true;
					}
					else {
						dogptr->m_Dog_OrderNow = false;
						m_Dog_OrderNow = false;
					}
				}
			}
		}
	}


	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance() {
		static shared_ptr<DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj) {
		
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj) {
		Obj->MoveMotion();
			Obj->PlayerToDog_Order();
	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}



}
//end basecross

