
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::OnCreate(){
		try {
			wstring DataDir;
			App::GetApp()->GetDataDirectory(DataDir);
			//モデル
			//プレゼントボックスモデル
			auto ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"BOX.bmf");
			App::GetApp()->RegisterResource(L"BOX_MESH", ModelMesh);
			//ドアモデル
			ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Door.bmf");
			App::GetApp()->RegisterResource(L"DOOR_MESH", ModelMesh);
			//犬モデル
			ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Dog01.bmf");
			App::GetApp()->RegisterResource(L"DOG_MESH", ModelMesh);

	
			
			//最初のアクティブステージの設定
			ResetActiveStage<MenuStage>();
		}
		catch (...) {
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {
		if (event->m_MsgStr == L"ToGameStage") {
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage>();
		}
		else if (event->m_MsgStr == L"ToMenuStage") {
			//最初のアクティブステージの設定
			ResetActiveStage<MenuStage>();
		}
		else if (event->m_MsgStr == L"ToClearStage") {
			//ゲームクリアステージの設定
			ResetActiveStage<ClearStage>();
		}
	}



}
//end basecross
