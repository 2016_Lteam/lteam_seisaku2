/*!
@file Dog.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	class Dog : public GameObject;
	//	用途: 犬
	//--------------------------------------------------------------------------------------
	class Dog : public GameObject {
		shared_ptr< StateMachine<Dog> >  m_StateMachine;	//ステートマシーン

		//犬の大きさ
		Vector3 m_Scale;
		//犬の回転
		Vector3 m_Rot;
		//犬の位置
		Vector3 m_Pos;

		//犬の座標を格納
		Vector3 m__DogPos;
		//Playerの座標を格納
		Vector3 m_Player_BeforePos;

		//移動量を格納する変数
		float m_Move_Distance = 0;

		//検索をかけたプレゼントボックス
		shared_ptr<PresentBoxModel> m_pb_storage;

		//次に向かうパスの情報を格納
		shared_ptr<RoadPath> m_rp_storage;
		
		//パスの移動履歴を格納
		vector<shared_ptr<RoadPath>> m_Path_History;

		//犬と検索したパス間が移動不可か判断
		bool m_Hindrance = false;

	public:
		//構築と破棄
		Dog(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Dog() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Dog> > GetStateMachine() const {
			return m_StateMachine;
		}

		//Playerの前のターンの座標を取得
		void SetPlayer_BeforePos(Vector3 Player_Pos);
		//Playerが移動したら犬もその後ろをついていく
		//void Player_FollowMove();

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		//犬固有関数群
		//一歩をスムーズにするための関数
		void Dog_Move_Now(int MD);

		//一番近くのプレゼントボックスを探す関数(中身ありの物を検索させる予定)
		void Search_Treasure();

		//犬の現在地がどのパスの座標とも一致しなかった場合、一番近くのパスの位置を検索する関数
		void Dog_SearchNearpath();

		//自分とパスの座標情報を取得し移動方向を決める(X軸とY軸を合わせる感じー)
		void DogToPath_MoveAngle();

		//犬固有変数群
		//犬が現在動いているかどうか
		bool m_Dog_Walk = false;

		//犬が現在動いているか(探索ステート中)
		bool m_Dog_SWalk = false;

		//プレイヤーの移動方向を管理する変数
		int m_Move_Direction = 0;

		//命令を受けているかどうか
		bool m_Dog_OrderNow = false;

		//プレゼントボックスにをターゲットしているか
		bool m_TargetTreasure = false;

		//現在地がパスの位置かどうか
		bool m_DogOnthePath = false;

		//m_Path_Historyの中身の数を格納(という名目のカウンター)
		int m_phCounter = 0;

		//目標地点をパスからプレゼントボックスに変更するフラグ
		bool m_PathToPresentBox = false;

	};

	//--------------------------------------------------------------------------------------
	//	class D_DefaultState : public ObjState<Dog>;
	//	用途: 命令待ち状態
	//--------------------------------------------------------------------------------------
	class D_DefaultState : public ObjState<Dog>
	{
		D_DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<D_DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Dog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Dog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Dog>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SearchState : public ObjState<Dog>;
	//	用途: 探索状態
	//--------------------------------------------------------------------------------------
	class SearchState : public ObjState<Dog>
	{
		SearchState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<SearchState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Dog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Dog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Dog>& Obj)override;
	};



}
//end basecross

