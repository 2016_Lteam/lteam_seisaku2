/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark::MultiSpark(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark::~MultiSpark() {}

	//初期化
	void MultiSpark::OnCreate() {
	}


	void MultiSpark::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.y * 5.0f,
				rParticleSprite.m_LocalPos.z * 5.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	//--------------------------------------------------------------------------------------
	///	半透明のスプライト
	//--------------------------------------------------------------------------------------
	TraceSprite::TraceSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0)
	{}
	TraceSprite::~TraceSprite() {}
	void TraceSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列
		m_BackupVertices = {
			{ VertexPositionColor(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f,0.0f,0.0f,0.0f)) },
			{ VertexPositionColor(Vector3(HelfSize, HelfSize, 0), Color4(0.0f, 1.0f, 0.0f, 0.0f)) },
			{ VertexPositionColor(Vector3(-HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 1.0f, 0.0f)) },
			{ VertexPositionColor(Vector3(HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 0, 0.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCSpriteDraw>(m_BackupVertices, indices);
	}
	void TraceSprite::OnUpdate() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		if (m_TotalTime >= XM_PI) {
			m_TotalTime = 0;
		}
		vector<VertexPositionColor> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Color4 col = m_BackupVertices[i].color;
			col.w = sin(m_TotalTime);
			auto v = VertexPositionColor(
				m_BackupVertices[i].position,
				col
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PCSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);

	}


	//--------------------------------------------------------------------------------------
	///	壁模様のスプライト
	//--------------------------------------------------------------------------------------
	WallSprite::WallSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}

	WallSprite::~WallSprite() {}
	void WallSprite::OnCreate() {
		float HelfSize = 0.5f;

		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>();
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}


	//--------------------------------------------------------------------------------------
	///	スイッチのスプライト
	//--------------------------------------------------------------------------------------
	SwitchSprite::SwitchSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}

	SwitchSprite::~SwitchSprite() {}
	void SwitchSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f,1.0f,1.0f,1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize, 0), Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(5.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0f, 5.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 0, 1.0f), Vector2(5.0f, 5.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}



	//--------------------------------------------------------------------------------------
	///	スクロールするスプライト
	//--------------------------------------------------------------------------------------
	ScrollSprite::ScrollSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0)
	{}

	ScrollSprite::~ScrollSprite() {}
	void ScrollSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HelfSize, HelfSize, 0), Vector2(0.0f, 0.0f)) },
			{ VertexPositionTexture(Vector3(HelfSize, HelfSize, 0), Vector2(4.0f, 0.0f)) },
			{ VertexPositionTexture(Vector3(-HelfSize, -HelfSize, 0), Vector2(0.0f, 1.0f)) },
			{ VertexPositionTexture(Vector3(HelfSize, -HelfSize, 0), Vector2(4.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}
	void ScrollSprite::OnUpdate() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		if (m_TotalTime > 1.0f) {
			m_TotalTime = 0;
		}
		vector<VertexPositionTexture> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Vector2 UV = m_BackupVertices[i].textureCoordinate;
			if (UV.x == 0.0f) {
				UV.x = m_TotalTime;
			}
			else if (UV.x == 4.0f) {
				UV.x += m_TotalTime;
			}
			auto v = VertexPositionTexture(
				m_BackupVertices[i].position,
				UV
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);
	}


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox() {}

	//初期化
	void FixedBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		PtrObb->SetUpdateActive(false);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"WALL_TX");

		SetDrawLayer(1);
	}


	//--------------------------------------------------------------------------------------
	//	class PresentBoxModel : public GameObject;
	//	用途: プレゼントボックスモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	PresentBoxModel::PresentBoxModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	PresentBoxModel::~PresentBoxModel() {}

	//初期化
	void PresentBoxModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(4.0f, 4.0f, 4.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"BOX_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrRegid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGravity = AddComponent<Gravity>();


		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		PtrDraw->SetMeshResource(L"BOX_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Default", 0, 15, false, 15.0f);
		PtrDraw->ChangeCurrentAnimation(L"Default");



		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"GameBgm.wav";
		App::GetApp()->RegisterWav(L"GAMEBGM", strMusic);

		//オーディオの初期化
		m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
		m_AudioObjectPtr->AddAudioResource(L"GAMEBGM");
		m_AudioObjectPtr->Start(L"GAMEBGM", XAUDIO2_LOOP_INFINITE, 0.2f);


		//Cubeの衝突判定をつける
		//auto CollPtr = AddComponent<CollisionObb>();
		SetDrawLayer(1);

	}

	void PresentBoxModel::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto P_Pos = PlayerPtr->GetComponent<Transform>()->GetPosition();

		auto myPos = GetComponent<Transform>()->GetPosition();
		//プレゼントボックスの前後左右にプレイヤーか犬がいたら、プレイヤーと犬はアイテムを取得する
		for (int i = 0; i < 2; i++) {
			if (i > 0) {
				P_Pos = DogPtr->GetComponent<Transform>()->GetPosition();
			}
			if (P_Pos.x == myPos.x && P_Pos.z == myPos.z + 1 ||
				P_Pos.x == myPos.x && P_Pos.z == myPos.z - 1 ||
				P_Pos.x == myPos.x + 1 && P_Pos.z == myPos.z ||
				P_Pos.x == myPos.x - 1 && P_Pos.z == myPos.z) {
				m_OpenBox = true;
			}
		}

		if (m_OpenBox) {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			float ElapsedTime = App::GetApp()->GetElapsedTime();

			if (PtrDraw->UpdateAnimation(ElapsedTime)) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToClearStage");
				m_AudioObjectPtr->Stop(L"GAMEBGM");
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: ドアモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel::DoorModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	DoorModel::~DoorModel() {}

	//初期化
	void DoorModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DOOR_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrRegid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGravity = AddComponent<Gravity>();


		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		PtrDraw->SetMeshResource(L"DOOR_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Open", 0, 25, false,15.0f);
		PtrDraw->AddAnimation(L"Close", 0, 1, false, 15.0f);
		PtrDraw->ChangeCurrentAnimation(L"Close");


		//Cubeの衝突判定をつける
		//auto CollPtr = AddComponent<CollisionObb>();

		auto DG = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		m_DoorNum = DG->size();

		SetDrawLayer(1);

		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Tobira.wav";
		App::GetApp()->RegisterWav(L"TOBIRA", strMusic);

	}

	void DoorModel::OnUpdate() {
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		
			PtrDraw->UpdateAnimation(ElapsedTime);


				if (m_Open&&!m_ChangeAnimation) {
					PtrDraw->ChangeCurrentAnimation(L"Open");
					m_ChangeAnimation = true;
					//オーディオの初期化
					m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
					m_AudioObjectPtr->AddAudioResource(L"TOBIRA");
					m_AudioObjectPtr->Start(L"TOBIRA", 0.5f);
				}
				else if(!m_Open){
					PtrDraw->ChangeCurrentAnimation(L"Close");
					m_ChangeAnimation = false;
				}

	}

	//--------------------------------------------------------------------------------------
	//	class SwitchModel : public GameObject;
	//	用途: スイッチモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SwitchModel::SwitchModel(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	SwitchModel::~SwitchModel() {}

	//初期化
	void SwitchModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		////影をつける
		//auto ShadowPtr = AddComponent<Shadowmap>();
		//ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SWITCH_TX");

		SetAlphaActive(true);

		auto SG = GetStage()->GetSharedObjectGroup(L"SwitchGroup");
		m_SwitchNum = SG->size();

		SetDrawLayer(1);


	}
	void SwitchModel::OnUpdate() {
		//犬とプレイヤーの位置情報を取得
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto D_Pos = DogPtr->GetComponent<Transform>()->GetPosition();
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto P_Pos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//スイッチ自身の位置情報も用意しておく
		auto m_Pos = GetComponent<Transform>()->GetPosition();
		//同じ数字のドアを格納するための変数
		shared_ptr<DoorModel> DoorPtr;
		auto DG = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		for (auto v : DG->GetGroupVector()) {
			auto dptr = dynamic_pointer_cast<DoorModel>(v.lock());
			if (m_SwitchNum == dptr->m_DoorNum) {
				DoorPtr = dptr;
				break;
			}
		}
		//スイッチの上にプレイヤーまたは犬が乗っていれば
		if (((m_Pos.x == D_Pos.x) && (m_Pos.z == D_Pos.z)) || ((m_Pos.x == P_Pos.x) && (m_Pos.z == P_Pos.z))) {
			//対応したドアを開く
			DoorPtr->m_Open = true;
		}
		else {
			DoorPtr->m_Open = false;
		}

	}




	//--------------------------------------------------------------------------------------
	//	class RoadPath : public GameObject;
	//	用途: 経路探索用のパス
	//--------------------------------------------------------------------------------------
	RoadPath::RoadPath(const shared_ptr<Stage>& StagePtr, const Vector3& Position) :
		GameObject(StagePtr),
		m_Pos(Position)
	{}
	RoadPath::~RoadPath() {}

	void RoadPath::OnCreate() {
		AddComponent<Transform>()->SetPosition(m_Pos);
	}

}
//end basecross
