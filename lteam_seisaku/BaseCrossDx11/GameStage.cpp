/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	void MenuStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"trace.png";
		/*App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		strTexture = DataDir + L"sky.jpg";
		App::GetApp()->RegisterTexture(L"SKY_TX", strTexture);*/
		strTexture = DataDir + L"start.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = DataDir + L"result.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
	}

	//ビューとライトの作成
	void MenuStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//タイトルのスプライト作成
	void MenuStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"TITLE_TX", false,
			Vector2(w, h), Vector2(0, 0));
	}

	//スイッチスプライト作成
	void MenuStage::CreateSwitchSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth()) / 2.0f;
		float h = static_cast<float>(App::GetApp()->GetGameHeight()) / 2.0f;

		AddGameObject<WallSprite>(L"SWITCH_TX", false,
			Vector2(w, h), Vector2(0, 0));
	}


	//スクロールするスプライト作成
	void MenuStage::CreateScrollSprite() {
		/*AddGameObject<ScrollSprite>(L"TRACE_TX", true,
		Vector2(512.0f, 128.0f), Vector2(0, 0));*/
	}


	void MenuStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();
			//スクロールするスプライト作成
			CreateScrollSprite();
			////スイッチのスプライト作成
			//CreateSwitchSprite();
			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Title.wav";
			App::GetApp()->RegisterWav(L"TITLE", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"TITLE");
			m_AudioObjectPtr->Start(L"TITLE", XAUDIO2_LOOP_INFINITE, 0.2f);
		}
		catch (...) {
			throw;
		}
	}

	//更新
	void MenuStage::OnUpdate() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage");
				m_AudioObjectPtr->Stop(L"TITLE");
			}
		}
	}




	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void GameStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"trace.png";
		App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		strTexture = DataDir + L"Floor.png";
		App::GetApp()->RegisterTexture(L"FLOOR_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Wall.png";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = DataDir + L"Blue.jpg";
		App::GetApp()->RegisterTexture(L"BLUE_TX", strTexture);
		strTexture = DataDir + L"DogColor.png";
		App::GetApp()->RegisterTexture(L"DOG_COLOR_TX", strTexture);
		strTexture = DataDir + L"switch_Door.png";
		App::GetApp()->RegisterTexture(L"SWITCH_TX", strTexture);
		strTexture = DataDir + L"BG.png";
		App::GetApp()->RegisterTexture(L"BACKGROUND_TX", strTexture);


		
		//プレイヤーモデル
		//ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Player.bmf");
		//App::GetApp()->RegisterResource(L"PLAYER_MESH", ModelMesh);

	}



	//ビューとライトの作成
	void GameStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 15.0f, -25.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}


	//プレートの作成
	void GameStage::CreatePlate() {
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(200.0f, 200.0f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
		);
		PtrTrans->SetScale(20.0f, 20.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(-0.5f, 0.0f, -0.5f);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"FLOOR_TX");

		auto PtrCamera = dynamic_pointer_cast<LookAtCamera>(GetView()->GetTargetCamera());
		if (PtrCamera) {
			PtrCamera->SetTargetObject(Ptr);
		}

		Ptr->SetDrawLayer(1);


		//背景の描画
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		auto bg = AddGameObject<WallSprite>(L"BACKGROUND_TX", false,
			Vector2(w, h), Vector2(0, 0));
		bg->SetDrawLayer(0);
	}


	//固定のボックスの作成
	void GameStage::CreateFixedBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(20.0f, 2.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.0f, 10.0f)
			},
			{
				Vector3(20.0f, 2.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.0f, -10.0f)
			},

			{
				Vector3(1.0f, 2.0f, 20.0f),
				Vector3(0, 0, 0),
				Vector3(10.0f, 0.0f, 0.0f)
			},
			{
				Vector3(1.0f, 2.0f, 20.0f),
				Vector3(0, 0, 0),
				Vector3(-10.0f, 0.0f, 0.0f)
			},
		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<FixedBox>(v[0], v[1], v[2]);
		}
	}

	//プレゼントボックスの作成
	void GameStage::CreatePresentBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(0.6f, 0.6f, 0.6f),
				Vector3(0, 0, 0),
				Vector3(-5.0f, 0.5f, 0.0f)
			},
		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<PresentBoxModel>(v[0], v[1], v[2]);
		}
	}

	//ドアの作成
	void GameStage::CreateDoor() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(0.1f, 0.1f, 1.0f),
				Vector3(0, 0, 0),
				Vector3(-8.0f, 0.5f, 0.0f)
			},
		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<DoorModel>(v[0], v[1], v[2]);
		}
	}

	//プレイヤーの作成
	void GameStage::CreatePlayer() {
		//プレーヤーの作成
		//auto PlayerPtr = AddGameObject<Player>();
		//シェア配列にプレイヤーを追加
		//SetSharedGameObject(L"Player", PlayerPtr);
	}

	//犬の作成
	void GameStage::CreateDog() {
		//犬の作成
		//auto DogPtr = AddGameObject<Dog>();
		//シェア配列を犬を追加
		//SetSharedGameObject(L"Dog", DogPtr);
	}

	//半透明のスプライト作成
	void GameStage::CreateTraceSprite() {
		AddGameObject<TraceSprite>(true,
			Vector2(200.0f, 200.0f), Vector2(-500.0f, -280.0f));
	}


	//壁模様のスプライト作成
	void GameStage::CreateWallSprite() {
		AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(200.0f, 200.0f), Vector2(500.0f, -280.0f));
	}


	//スイッチのスプライト作成
	void GameStage::CreateSwitchSprite() {
		AddGameObject<WallSprite>(L"SWITCH_TX", false,
			Vector2(200.0f, 200.0f), Vector2(500.0f, -280.0f));
	}


	//スクロールするスプライト作成
	void GameStage::CreateScrollSprite() {
		AddGameObject<ScrollSprite>(L"TRACE_TX", true,
			Vector2(160.0f, 40.0f), Vector2(500.0f, -280.0f));
	}





	//スパークの作成
	void GameStage::CreateSpark() {
		auto MultiSparkPtr = AddGameObject<MultiSpark>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	void GameStage::CreateStageObject() {
		////Csvファイル
		//wstring Filename = App::GetApp()->m_wstrRelativeDataPath + L"Map_Csv\\" + L"data" +L".csv";
		////ローカル上にCSVファイルクラスの作成
		//CsvFile GameStageCsv(Filename);
		//if (!GameStageCsv.ReadCsv()) {
		//	//ファイルが存在しなかった
		//	//初期化失敗
		//	throw BaseException(
		//		L"CSVファイルがありません。",
		//		Filename,
		//		L"選択された場所にはアクセスできません"
		//	);
		//}
		//const int iDataSizeRow = 0;		//データが0行目から始まるようの定数
		//vector< wstring > StageMapVec;	//ワーク用のベクター配列
		//								//iDataSizeRowのデータを抜き取りベクター配列に格納
		//GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//////行、列の取得
		//line_size = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row  = 行
		//col_size = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col　= 列
		//															   //マップデータ配列作成
		//m_MapDataVec = vector< vector<size_t> >(line_size, vector<size_t>(col_size));	//列の数と行の数分作成
		//																					//配列の初期化
		//for (size_t i = 0; i < line_size; i++) {
		//	for (size_t j = 0; j < col_size; j++)
		//	{
		//		m_MapDataVec[i][j] = 0;								//size_t型のvector配列を０で初期化			
		//	}
		//}
		////1行目からステージが定義されている
		//const int iDataStartRow = 1;
		////assert(m_sRowSize > 0 && "行が０以下なのでゲームが開始できません");
		//if (line_size > 0) {
		//	for (size_t i = 0; i < line_size; i++) {
		//		GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
		//		for (size_t j = 0; j < col_size; j++) {					//列分ループ処理
		//			const int iNum = _wtoi(StageMapVec[j].c_str());			//列から取得したwstring型をintに変換→格納	
		//		//マップデータ配列に格納
		//			m_MapDataVec[i][j] = iNum;
		//			//配置されるオブジェクトの基準スケール
		//			float ObjectScale = 1.0f;
		//			//基準Scale
		//			Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
		//			Vector3 Pos(static_cast<float>(j), 0.5, static_cast<float>(i));
		//			switch (iNum) {
		//			case 1:
		//				AddGameObject<FixedBox>(Scale, Vector3(0, 0, 0), Pos);
		//				break;
		//			}
		//		}
		//	}
		//}

		//ゲームプログラミングのファイル
		//// ファイルからマップデータを読み込む	------------------------------------------
		//FILE*	fp;
		////fp = fopen("map.csv", "r");
		//if (!fopen_s(&fp, fileName, "r")) {// 読み取り専用で開く
		//	int a;	// 読み込んだデータを一時的に保存する変数
		//	int c = 0, r = 0;	// マップ用の二次元配列の添え字
		//	char digit[100] = { 0 };
		//	int d_cnt = 0;
		//	do {
		//		a = fgetc(fp);	// ファイルから１文字読み込む
		//		if (isdigit(a)) {	// 読み込んだ文字が整数か判断
		//							//fgetcで読み込んだ文字をdigitに入れていく
		//			digit[d_cnt++] = a;
		//		}
		//		else {
		//			digit[d_cnt] = '\0';
		//			d_cnt = 0;
		//			//digitのデータを数値に変換してマップに入れる 
		//			//map[r][c++] = atoi(digit);
		//			map[r * col + c] = atoi(digit);
		//			c++;
		//			if (c >= col) {
		//				c = 0;
		//				r++;
		//				if (r>row) {
		//					break;
		//				}
		//			}
		//		}
		//	} while (a != EOF);	// ファイルの終了を示す値(EOF, End Of File)を読み込んだら終了
		//	fclose(fp);	// ファイルを閉じる
		//}
		//	------------------------------------------------------------------------------

		int MapSize = 20;

		//ゲームオブジェクトの大きさ、回転、位置
		Vector3 GO_Scale = Vector3(0, 0, 0);
		Vector3 GO_Rot = Vector3(0, 0, 0);
		Vector3 GO_Pos = Vector3(0, 0, 0);

		//プレイヤーと犬以外のオブジェクトを格納するオブジェクトグループ
		auto OGO = CreateSharedObjectGroup(L"Other_GameObject");

		//プレイヤーと犬の情報を格納するオブジェクトグループ
		auto PandD = CreateSharedObjectGroup(L"PlayerAndDog");

		//プレゼントボックスのグループ格納用
		auto PBG = CreateSharedObjectGroup(L"PresentBoxGroup");

		//ドアのグループ格納用
		auto DG = CreateSharedObjectGroup(L"DoorGroup");

		//スイッチのグループ格納用
		auto SG = CreateSharedObjectGroup(L"SwitchGroup");

		//経路探索用のパス群
		auto Route_search = CreateSharedObjectGroup(L"RoadPathGroup");

		shared_ptr<PresentBoxModel> pbptr;


		const int map[20][20] = {
		//  0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19
			1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,
			1,	4,	0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	5,	1,	1,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	99,	1,
			1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	0,	6,	0,	0,	0,	0,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	99,	0,	0,	0,	0,	99,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	2,	99,	1,
			1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	3,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
			1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,
			1,	99,	0,	0,	0,	0,	99,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	99,	1,
			1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,
			1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,
		};

		for (int i = 0; i < MapSize; i++) {
			for (int j = 0; j < MapSize; j++) {
				GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
				GO_Rot = Vector3(0, 0, 0);
				GO_Pos = Vector3((float)j - MapSize / 2, 0.0, (float)i - MapSize / 2);
				switch (map[j][i]) {
					//1なら壁を配置する
				case 1:
					GO_Scale = Vector3(1.0f, 2.0f, 1.0f);
					OGO->IntoGroup(AddGameObject<FixedBox>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//2ならプレイヤーを配置する
				case 2:
					SetSharedGameObject(L"Player", AddGameObject<Player>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//3なら犬を配置する
				case 3:
					SetSharedGameObject(L"Dog", AddGameObject<Dog>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//4ならプレゼントボックスを配置する
				case 4:
					GO_Scale = Vector3(0.6f, 0.6f, 0.6f);
					pbptr = AddGameObject<PresentBoxModel>(GO_Scale, GO_Rot, GO_Pos);
					OGO->IntoGroup(pbptr);
					PBG->IntoGroup(pbptr);
					break;
					//5ならドアを配置する
				case 5:
					GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
					GO_Rot = Vector3(0, -3.14/2, 0);
					DG->IntoGroup(AddGameObject<DoorModel>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//6ならスイッチを配置する
				case 6:
					GO_Scale = Vector3(1.0f, 0.01f, 1.0f);
					GO_Rot = Vector3(0, 0, 0);
					SG->IntoGroup(AddGameObject<SwitchModel>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//経路探索用パスの配置
				case 99:
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
				}
			}
		}

			
	}




	void GameStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			////固定のボックスの作成
			//CreateFixedBox();
			////半透明のスプライト作成
			//CreateTraceSprite();
			////壁模様のスプライト作成
			//CreateWallSprite();
			////スクロールするスプライト作成
			//CreateScrollSprite();
			////スパークの作成
			//CreateSpark();
			//プレーヤーの作成
			//CreatePlayer();
			//犬の作成
			//CreateDog();
			//プレゼントボックス(仮)
			//CreatePresentBox();
			//ドアの作成
			//CreateDoor();
			//ステージオブジェクトの作成(CSV)
			CreateStageObject();

		}
		catch (...) {
			throw;
		}
	}

	void GameStage::OnUpdate() {

	}

	//--------------------------------------------------------------------------------------
	//	クリアステージクラス実体
	//--------------------------------------------------------------------------------------


	void ClearStage::CreateResourses() {
		wstring DataDir;
		wstring strTexture = DataDir + L"result.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
	}

	//ビューとライトの作成
	void ClearStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//タイトルのスプライト作成
	void ClearStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"RESULT_TX", false,
			Vector2(w, h), Vector2(0, 0));
	}


	void ClearStage::OnCreate() {
		try {
			//リソースの作成
			//CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();

			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"1.wav";
			App::GetApp()->RegisterWav(L"11", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"11");
			m_AudioObjectPtr->Start(L"11", XAUDIO2_LOOP_INFINITE, 0.2f);
		}
		catch (...) {
			throw;
		}
	}

	//更新
	void ClearStage::OnUpdate() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
				m_AudioObjectPtr->Stop(L"11");
			}
		}
	}

}
//end basecross
