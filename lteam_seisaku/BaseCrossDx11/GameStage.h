/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class MenuStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//スクロールするスプライト作成
		void CreateScrollSprite();
		//スイッチのスプライト作成
		void CreateSwitchSprite();
	public:
		//構築と破棄
		MenuStage() :Stage() {}
		virtual ~MenuStage() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};


	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//追いかけるオブジェクトの作成
		void CreateSeekObject();
		//固定のボックスの作成
		void CreateFixedBox();
		//上下移動しているボックスの作成
		void CreateMoveBox();
		//ヒットする球体の作成
		void CreateSphere();
		//半透明のスプライト作成
		void CreateTraceSprite();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//スイッチのスプライト
		void CreateSwitchSprite();
		//スクロールするスプライト作成
		void CreateScrollSprite();
		//左上で回転する立方体
		void CreateRollingCube();
		//形状が変わる球体
		void CreateTransSphere();
		//スパークの作成
		void CreateSpark();
		//プレイヤーの作成
		void CreatePlayer();
		//犬の作成
		void CreateDog();
		//プレゼントボックス(仮)
		void CreatePresentBox();
		//ドア(仮)
		void CreateDoor();
		//ステージオブジェクトの作成
		void CreateStageObject();


		//CSV読み込み 
		void LoadMAPCSV();
		UINT m_StageNum = 1;
		int *m_Stagearr;				//ステージ配列
		Vector2 m_StageSize;			//ステージサイズ
		Vector3 m_PlayerinitPos;		//プレイヤーの初期位置
		//Csv用変数
		size_t line_size;
		size_t col_size;


		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;
	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//	クリアステージクラス
	//--------------------------------------------------------------------------------------
	class ClearStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
	public:
		//構築と破棄
		ClearStage() :Stage() {}
		virtual ~ClearStage() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

	

}
//end basecross

