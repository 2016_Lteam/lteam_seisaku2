/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン

		//プレイヤーの大きさ
		Vector3 m_Scale;
		//プレイヤーの回転
		Vector3 m_Rot;
		//プレイヤーの位置
		Vector3 m_Pos;

		//Playerの前のターンの座標を格納
		Vector3 m_BeforePos;

		//最初の一歩を踏み出したかフラグ
		bool m_first_Walk = false;

		//カメラの角度を取得したい変数
		float m_C_Eye = 0;

		//プレイヤーが現在動いているかどうかのフラグ
		bool m_Player_Walk = false;

		//移動量を格納する変数
		float m_Move_Distance = 0;

	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Player() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player> > GetStateMachine() const {
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveMotion();
		//一歩をスムーズにするための関数
		void Player_Move_Now(int MD);
		//プレイヤーが前のターンから移動したか確認する関数
		void Confirmation_Pos();
		//プレイヤーが指示を出す関数
		void PlayerToDog_Order();
		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		//プレイヤーの移動方向を管理する変数
		int m_Move_Direction = 0;

		//プレイヤーの移動速度を設定
		float Move_Speed = 0.05f;

		//犬の移動する向きを決める変数
		int m_Dog_MoveAngle = 0;

		//現在犬は命令を受けているかのフラグ
		bool m_Dog_OrderNow = false;
	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};



}
//end basecross

